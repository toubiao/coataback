
var _ = require('lodash');
var mongo = require('mongoskin');
var Q = require('q');
var mongoose = require('mongoose');
var produits = mongoose.model('Produit');
var categories = mongoose.model('Categorie');
var tailles = mongoose.model('Taille');
const NodeCache = require( "node-cache" );
const localStorage = new NodeCache();
var menu = require("../test.json");

var service = {};

service.getAll = getAll;
service.getById = getById;
service.create = create;
service.update = update;
service.delete = _delete;
service.init = getAllAndStore;
module.exports = service;


function getAll() {
    const deferred = Q.defer();
    deferred.resolve(menu);
    return deferred.promise;
  }

function getAllAndStore(){
  var menu = [];

  categories.find({}).sort('-nom').exec(async function(err,cats){
      var index = 0;
      cats.forEach(function(cat){
           console.log(cat);
         makeMenuProCategory(cat,function(catMenu){
             menu.push(catMenu);
             index++;
             if (index == cats.length){
             localStorage.set("menu", menu, function( err, success ){
               console.log('---------------------------------------------------------');
               console.log(menu);
                if( !err && success ){
                  console.log(success);
                }
                console.log('---------------------------------------------------------');
                console.log(err);
              });
            }
         });
       });
  });
}
function makeMenuProCategory(cat,callback){
    var catMenu = {};

    catMenu.categorieName = cat.nom;
    catMenu.hasLegume = cat.hasLegume;
    catMenu.hasSupp = cat.hasSupp;
    catMenu.hasSauce = cat.hasSauce;
    catMenu.produits = [];

    produits.find({categorieId:cat._id}).sort('-nom').exec(function (err, prods) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        var index = 0;
        prods.forEach(function(prod){

            tailles.find({produitId:prod._id}, function(err,taille){
                index++;
                var prix = (taille.length > 1) ? taille.map((t) => {return t.prix}).join(' / ') : taille[0].prix;
                catMenu.produits.push(
                   {'id':prod._id,'nom':prod.nom,'nomCategorie': catMenu.categorieName,'ingredients':prod.ingredients,'prix':prix,'tailles':taille,
                   'suppLivraison':prod.suppLivraison}
                );
                if(index == prods.length)
                    callback(catMenu);

            });
        });
    });
}

function getById(_id) {
    var deferred = Q.defer();

    produits.findById(_id, function (err, prod) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (prod) {
            // return user (without hashed password)
            deferred.resolve(prod);
        } else {
            // user not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function create(prodParam) {
    var deferred = Q.defer();

    // validation
    produits.findOne(
        { nom: userParam.nom },
        function (err, prod) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (prod) {
                deferred.reject('produit "' + userParam.nom + '" deja pris');
            } else {
                createProd();
            }
        });

    function createProd() {
        produits.create(user, function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
          });
    }

    return deferred.promise;
}

function update(_id, prodParam) {
    var deferred = Q.defer();

    // validation
    produits.findById(_id, function (err, prod) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (prod.nom !== prodParam.nom) {
            // username has changed so check if the new username is already taken
            produits.findOne(
                { username: prodParam.nom },
                function (err, prod) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (prod) {
                        // username already exists
                        deferred.reject('Produit "' + req.body.nom + '" is already taken');
                    } else {
                        updateProd();
                    }
                });
        } else {
            updateProd();
        }
    });

    function updateProd() {
        // fields to update
        var set = {
            nom: userParam.nom,
        };

        produits.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    produits.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
