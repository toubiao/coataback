
var _ = require('lodash');
var mongo = require('mongoskin');
var Q = require('q');
var mongoose = require('mongoose');
var commandes = mongoose.model('Commande');
var detComm = mongoose.model('CommandeDetail');
var jwtService = require('./jwt.service');
var users = mongoose.model('User');

var service = {};


service.getById = getById;
service.create = create;
service.delete = _delete;

module.exports = service;




function getById(_id) {
    var deferred = Q.defer();

    commandes.findById(_id, function (err, prod) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (prod) {
            // return user (without hashed password)
            deferred.resolve(prod);
        } else {
            // user not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function create(commParam,userId) {
    var deferred = Q.defer();
    // 2 maniere de recuperer l'id
    //var userId = jwtService.decodeId(token);

    var commande = prepareObjectCommandeToDb(commParam);

      commandes.create(commande, function (err, doc) {
          if (err) deferred.reject(err.name + ': ' + err.message);

          deferred.resolve();
        });

     function prepareObjectCommandeToDb(tmpComm){
      var size = tmpComm.items.length;
      var newCommande = {userId: mongo.helper.toObjectID(userId),
                        deliveryOption: (tmpComm.deliveryOptionId == 1 ) ? "take-away" : "Livraison",
                        prixTotal: tmpComm.grossTotal,
                        deliveryTime: tmpComm.checkoutTime,
                        delaiLivraison: tmpComm.checkoutTime,
                        totalLivraison: tmpComm.deliveryTotal};
      newCommande.commandeDetails = tmpComm.items;
      if (tmpComm.deliveryOptionId == 2){
        users.findById(mongo.helper.toObjectID(userId), function (err, user) {
          console.log(user);
          console.log(err);
              newCommande.deliveryAdress = user.adresse;

        });
      }

      //TODO: regarder si on supprime detailCommande
      var newDetailsCommande = [];
      for(var i = 0; i < size; i++){
        newDetailsCommande.push(prepareObjectDetailCommandeToDb(tmpComm.items[i]));
      }

      newCommande.commandeDetails = newDetailsCommande;
      return newCommande;
    }
    function prepareObjectDetailCommandeToDb(tmpDetComm){
        var newDetComm = {
          produitId: mongo.helper.toObjectID(tmpDetComm.productId),
          tailleId: mongo.helper.toObjectID(tmpDetComm.tailleId),
          supplements: _.forEach(tmpDetComm.supplementsId, function(id) { return mongo.helper.toObjectID(id) }),
          legumes: _.forEach(tmpDetComm.legumesId, function(id) { return mongo.helper.toObjectID(id) }),
          sauces: _.forEach(tmpDetComm.saucesId, function(id) { return mongo.helper.toObjectID(id) }),
          quantité: tmpDetComm.quantity,
          prix: tmpDetComm.prixItem
        }
        return newDetComm;
    }
    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    commandes.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
