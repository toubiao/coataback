var jwt = require('jsonwebtoken');
var config = require('config.json');
var service = {};


service.decodeId = _decodeId;

module.exports = service;


function _decodeId(token){

  return jwt.verify(token,config.secret).sub;
}
