﻿var config = require('config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var mongo = require('mongoskin');
var Q = require('q');
var mongoose = require('mongoose');

var users = mongoose.model('User');
const Nexmo = require('nexmo')

const nexmo = new Nexmo({
  apiKey: '77e4e979',
  apiSecret: 'jgsAxmX0qnB80T7o'
})

var service = {};

service.authenticate = authenticate;
service.getAll = getAll;
service.getById = getById;
service.create = create;
service.update = update;
service.delete = _delete;

module.exports = service;

var googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyByc_gKi4tXLKN6M3-rRe9IUzdYIgC8KkU'
});

function authenticate(mail, password) {
    var deferred = Q.defer();

    users.findOne({ mail: mail,confirmed:true }, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user && bcrypt.compareSync(password, user.hash)) {
            // authentication successful
            deferred.resolve({
                _id: user._id,
                mail: user.mail,
                prenom: user.prenom,
                nom: user.nom,
                adresse: user.adresse,
                numeroTel: user.numeroTel,
                token: jwt.sign({ sub: user._id,iat: Math.floor(Date.now() / 1000) - 30  }, config.secret, { expiresIn: '2h'})
            });
        } else {
            // authentication failed
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getAll() {
    var deferred = Q.defer();

    users.find({},function (err, users) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        // return users (without hashed passwords)
        users = _.map(users, function (user) {
            return _.omit(user, 'hash');
        });

        deferred.resolve(users);
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    users.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user) {
            // return user (without hashed password)
            deferred.resolve(_.omit(user, 'hash'));
        } else {
            // user not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function create(userParam) {
    var deferred = Q.defer();

    // validation
    users.findOne(
        { mail: userParam.mail },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (user) {
                // username already exists
                deferred.reject('mail "' + userParam.mail + '" is already taken');
            } else {
                createUser();
            }
        });

    function createUser() {

        // set user object to userParam without the cleartext password
        var user = _.omit(userParam, 'password');
        user = _.omit(user, 'adresse');
          // add hashed password to user object
        user.hash = bcrypt.hashSync(userParam.password, 10);

        googleMapsClient.geocode({
          address: userParam.adresse
        }, function(err, response) {
            if (!err) {
              user.adresse = response.json.results[0].formatted_address;
              user.codePostale = _.find(response.json.results[0].address_components, (cmp) => cmp.types[0] == 'postal_code' ).short_name;
              user.validationCode = Math.floor((Math.random() * 100000) + 1);
              user.confirmed = false;
              sendSmsValidation(user.validationCode);
              users.create(user, function (err, doc) {

                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
              });
            }else{
                console.log(err);
            }
        });

    }

    function sendSmsValidation(validationCode){
        const from = 'La Kebaberie';
        const to = '+41786016441';
        const text = 'Votre code de validation est : ' + validationCode;
        nexmo.message.sendSms(from, to, text);
    }

    return deferred.promise;
}

function update(_id, userParam) {
    var deferred = Q.defer();

    // validation
    users.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user.username !== userParam.username) {
            // username has changed so check if the new username is already taken
            users.findOne(
                { username: userParam.username },
                function (err, user) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (user) {
                        // username already exists
                        deferred.reject('Username "' + req.body.username + '" is already taken')
                    } else {
                        updateUser();
                    }
                });
        } else {
            updateUser();
        }
    });

    function updateUser() {
        // fields to update
        var set = {
            firstName: userParam.firstName,
            lastName: userParam.lastName,
            username: userParam.username,
        };

        // update password if it was entered
        if (userParam.password) {
            set.hash = bcrypt.hashSync(userParam.password, 10);
        }

        users.update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    users.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}
