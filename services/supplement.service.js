var _ = require('lodash');
var mongo = require('mongoskin');
var Q = require('q');
var mongoose = require('mongoose');
var Supplement = mongoose.model('Supplement');

var service = {};

service.getAll = getAll;
service.getById = getById;


module.exports = service;


function getAll() {
    const deferred = Q.defer();

    Supplement.find({},async function(err,supps){
        if (err) deferred.reject(err.name + ': ' + err.message);

        deferred.resolve(supps);
      });

    return deferred.promise;
}


function getById(_id) {
    var deferred = Q.defer();

    Supplement.findById(_id, function (err, taille) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (taille) {

            deferred.resolve(taille);
        } else {

            deferred.resolve();
        }
    });

    return deferred.promise;
}
