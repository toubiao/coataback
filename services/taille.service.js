var _ = require('lodash');
var mongo = require('mongoskin');
var Q = require('q');
var mongoose = require('mongoose');
var Taille = mongoose.model('Taille');

var service = {};

service.getAll = getAll;
service.getById = getById;


module.exports = service;


function getAll() {
    const deferred = Q.defer();

    Taille.find({},async function(err,tailles){
        if (err) deferred.reject(err.name + ': ' + err.message);

        deferred.resolve(tailles);
      });

    return deferred.promise;
}


function getById(_id) {
    var deferred = Q.defer();

    Taille.findById(_id, function (err, taille) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (taille) {

            deferred.resolve(taille);
        } else {

            deferred.resolve();
        }
    });

    return deferred.promise;
}
