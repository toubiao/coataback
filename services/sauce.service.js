var _ = require('lodash');
var mongo = require('mongoskin');
var Q = require('q');
var mongoose = require('mongoose');
var Sauce = mongoose.model('Sauce');

var service = {};

service.getAll = getAll;
service.getById = getById;


module.exports = service;


function getAll() {
    const deferred = Q.defer();

    Sauce.find({},async function(err,sauces){
        if (err) deferred.reject(err.name + ': ' + err.message);

        deferred.resolve(sauces);
      });

    return deferred.promise;
}


function getById(_id) {
    var deferred = Q.defer();

    Sauce.findById(_id, function (err, taille) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (taille) {

            deferred.resolve(taille);
        } else {

            deferred.resolve();
        }
    });

    return deferred.promise;
}
