var mongo = require('mongoskin');
var Q = require('q');
var mongoose = require('mongoose');
var Legume = mongoose.model('Legume');

var service = {};

service.getAll = getAll;
service.getById = getById;


module.exports = service;


function getAll() {
    const deferred = Q.defer();

    Legume.find({},async function(err,legumes){
        if (err) deferred.reject(err.name + ': ' + err.message);

        deferred.resolve(legumes);
      });

    return deferred.promise;
}


function getById(_id) {
    var deferred = Q.defer();

    Legume.findById(_id, function (err, legume) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (legume) {

            deferred.resolve(legume);
        } else {

            deferred.resolve();
        }
    });

    return deferred.promise;
}
