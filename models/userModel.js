'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * User Schema
 */
 var UserSchema = new Schema({
     nom: {
       type: String,
       trim: true,
       required: true
     },
     prenom: {
       type: String,
       trim: true,
       required: true
     },
     mail: {
       type: String,
       unique: true,
       trim: true,
       required: true
     },
     adresse: {
       type: String,
       required: true
     },
     codePostale: {
       type: String,
       required: true
     },
     validationCode: {
       type: Number,
       required: true
     },
     confirmed: {
       type: Boolean,
       required: true
     },
     numeroTel: {
       type: String,
       unique: true,
       lowercase: true,
       trim: true,
       required: true
     },
     hash: {
       type: String
     }
 });

module.exports = mongoose.model('User', UserSchema);
