'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * Produit Schema
 */
 var CommandeDetailSchema = new Schema({
     produitId: {
       type: mongoose.Schema.Types.ObjectId,
       ref:'Produit',
       required: true
     },
     tailleId: {
       type: mongoose.Schema.Types.ObjectId,
       ref:'Taille',
       required: true
     },
     supplements: [{type: mongoose.Schema.Types.ObjectId, ref:'Supplement'}],
     sauces: [{type: mongoose.Schema.Types.ObjectId, ref:'Sauce'}],
     quantité: {
       type: Number,
       required: true
     },
     prix: {
       type: Number,
       required: true
     }
 });

 var CommandeSchema = new Schema({
    userId: {type: Schema.Types.ObjectId, ref:'User',required: true},
    commandeDetails: [CommandeDetailSchema],
    deliveryOption: String,
    grossTotal: Number,
    checkoutTime: Number,
    deliveryTotal: Number
 });

module.exports = mongoose.model('Commande', CommandeSchema);
