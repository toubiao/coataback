'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * Produit Schema
 */

 var CategorieSchema = new Schema({
     nom: {
       type: String,
       trim: true,
       required: true,
       unique: true
     },
     hasSauce: {
       type: Boolean,
       required: true
     },
     hasLegume: {
       type: Boolean,
       required: true
     },
     hasSupp: {
       type: Boolean,
       required: true
     }
 });

module.exports = mongoose.model('Categorie', CategorieSchema);
