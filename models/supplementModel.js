'use strict';
'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * Produit Schema
 */

 var SupplementSchema = new Schema({
     nom: {
       type: String,
       trim: true,
       required: true,
       unique: true
     },
     prix: {
       type: Number,
       required: true
     }
 });

module.exports = mongoose.model('Supplement', SupplementSchema);
