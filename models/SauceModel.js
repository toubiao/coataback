'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * Produit Schema
 */

 var SauceSchema = new Schema({
     nom: {
       type: String,
       trim: true,
       required: true,
       unique: true
     }
 });

module.exports = mongoose.model('Sauce', SauceSchema);
