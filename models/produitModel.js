'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * Produit Schema
 */

 var ProduitSchema = new Schema({
     nom: {
       type: String,
       trim: true,
       required: true,
       unique: true
     },
     ingredients: {
       type: String,
       required: true
     },
     suppLivraison: {
       type: Number,
       required: true
     },
     categorieId: {
       type: mongoose.Schema.Types.ObjectId,
       ref:'Categorie',
       required: true
     }
 });

module.exports = mongoose.model('Produit', ProduitSchema);
