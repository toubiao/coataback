'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * Produit Schema
 */

 var TailleModelSchema = new Schema({
     nom: {
       type: String,
     },
     prix: {
       type: Number,
       required: true
     },
     produitId: {
       type: mongoose.Schema.Types.ObjectId,
       ref:'Produit',
       required: true
     }
 });
TailleModelSchema.index({nom: 1, produitId: 1}, {unique: true});

module.exports = mongoose.model('Taille', TailleModelSchema);
