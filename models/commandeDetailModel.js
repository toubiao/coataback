'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var arrayUniquePlugin = require('mongoose-unique-array');



/**
 * Produit Schema
 */

 var CommandeDetailSchema = new Schema({
     produitId: {
       type: mongoose.Schema.Types.ObjectId,
       ref:'Produit',
       required: true
     },
     tailleId: {
       type: mongoose.Schema.Types.ObjectId,
       ref:'Taille',
       required: true
     },
     supplements: [{type: mongoose.Schema.Types.ObjectId, ref:'Supplement'}],
     legumes: [{type: mongoose.Schema.Types.ObjectId, ref:'Legume'}],
     sauces: [{type: mongoose.Schema.Types.ObjectId, ref:'Sauce'}],
     quantité: {
       type: Number,
       required: true
     },
     prix: {
       type: Number,
       required: true
     }
 });
//CommandeDetailSchema.index({commandeId: 1, produitId: 1, tailleId: 1, supplements: 1}, {unique: true});
//CommandeDetailSchema.plugin(arrayUniquePlugin);

module.exports = mongoose.model('CommandeDetail', CommandeDetailSchema);
