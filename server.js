﻿require('rootpath')();
var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');
var config = require('config.json');
var helmet = require('helmet');
var mongoose = require('mongoose');
var User = require('./models/userModel');
var Supplement = require('./models/supplementModel');
var Categorie = require('./models/categorieModel');
var Sauce = require('./models/SauceModel');
var Legume = require('./models/LegumeModel');
var Commande = require('./models/commandeModel');
var Produit = require('./models/produitModel');
var TailleModel = require('./models/tailleModel');
var CommandeDetail = require('./models/commandeDetailModel');
var produitService = require('services/produit.service');






mongoose.connect(config.connectionString);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(helmet({
  frameguard: false
}))
// use JWT auth to secure the api, the token can be passed in the authorization header or querystring
app.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}).unless({ path: ['/users/authenticate', '/users/register','/produits/getMenu',
                  '/tailles/getAll','/sauces/getAll','/legumes/getAll',
                  '/supplements/getAll'] }));

// routes
app.use('/users', require('./controllers/users.controller'));
app.use('/produits', require('./controllers/produits.controller'));
app.use('/tailles', require('./controllers/taille.controller'));
app.use('/sauces', require('./controllers/sauce.controller'));
app.use('/commande', require('./controllers/commande.controller'));
app.use('/legumes', require('./controllers/legume.controller'));
app.use('/supplements', require('./controllers/supplement.controller'));

// error handler
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401).send('Invalid Token');
    } else {
        throw err;
    }
});

// start server
var port = process.env.NODE_ENV === 'production' ? 80 : 4000;
var server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
