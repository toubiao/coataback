﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var sauceService = require('services/sauce.service');

// routes
//router.post('/authenticate', authenticate);
//router.post('/register', register);
router.get('/getAll', getAll);


module.exports = router;


function getAll(req, res) {
    sauceService.getAll()
        .then(function (sauces) {
            res.send(sauces);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
