﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var tailleService = require('services/taille.service');

// routes
//router.post('/authenticate', authenticate);
//router.post('/register', register);
router.get('/getAll', getAll);


module.exports = router;


function getAll(req, res) {
    tailleService.getAll()
        .then(function (tailles) {
            res.send(tailles);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
