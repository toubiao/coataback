﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var supplementService = require('services/supplement.service');

// routes
//router.post('/authenticate', authenticate);
//router.post('/register', register);
router.get('/getAll', getAll);


module.exports = router;


function getAll(req, res) {
    supplementService.getAll()
        .then(function (supps) {
            res.send(supps);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
