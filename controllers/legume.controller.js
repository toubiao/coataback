﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var legumeService = require('services/legume.service');

// routes
//router.post('/authenticate', authenticate);
//router.post('/register', register);
router.get('/getAll', getAll);


module.exports = router;


function getAll(req, res) {
    legumeService.getAll()
        .then(function (legumes) {
            res.send(legumes);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
