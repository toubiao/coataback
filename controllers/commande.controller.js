﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var commandeService = require('services/commande.service');

// routes

router.get('/current', getCurrent);
router.post('/command', command);
router.put('/:_id', update);
router.delete('/:_id', _delete);

module.exports = router;

function command(req,res){
  commandeService.create(req.body,req.user.sub)
      .then(function () {
          res.json('success');
      })
      .catch(function (err) {
          res.status(400).send(err);
      });
}
function getCurrent(req, res) {
    produitService.getById(req.user.sub)
        .then(function (user) {
            if (user) {
                res.send(user);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
function update(req, res) {
    commandeService.update(req.params._id, req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function _delete(req, res) {
    commandeService.delete(req.params._id)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
