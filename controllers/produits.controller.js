﻿var config = require('config.json');
var express = require('express');
var router = express.Router();
var produitService = require('services/produit.service');

// routes
//router.post('/authenticate', authenticate);
//router.post('/register', register);
router.get('/getMenu', getMenu);
//router.get('/current', getCurrent);
//router.put('/:_id', update);
//router.delete('/:_id', _delete);

module.exports = router;


function getMenu(req, res) {
    produitService.getAll()
        .then(function (users) {
            res.send(users);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getCurrent(req, res) {
    produitService.getById(req.user.sub)
        .then(function (user) {
            if (user) {
                res.send(user);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function update(req, res) {
    produitService.update(req.params._id, req.body)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function _delete(req, res) {
    produitService.delete(req.params._id)
        .then(function () {
            res.json('success');
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
